-----------------
Coin Changer App
-----------------

This project was built with all the dependencies included in Create React App (https://github.com/facebookincubator/create-react-app) and Kube (https://imperavi.com/kube/) CSS Framework. To run this, you need to install NodeJS (https://nodejs.org/en/download/package-manager/) in your machine so you can run the necessary npm commands (https://docs.npmjs.com/cli/npm). 

Install all dependecies:
npm install

Run the included unit testings:
npm test -- --verbose

Run the app on port 7400:
npm start

NOTE: The default port `7400` is defined in `package.json` file.

------------------------------------
Run Included Build Without Compiling
------------------------------------

Alternatively, you could just download the already made `/build` folder to your computer and open `index.html` to run the app without compiling. I made changes to the image's relative paths on the `static/css/main.*.css` file, so that the images can be opened.

---------------
File Structure
---------------

- Main codes resides in `src/CoinChangerApp.js` & `CoinChangerHelper.js`.
- Test units resides in `src/CoinChangerApp.test.js` & `src/CoinChangerHelper.test.js`
- CSS codes that extends the Kube Framework resides in `src/style.css`

-------------------
How To Use The App
-------------------

- Type into the input and hit 'enter' or click the Find button to submit the form
- If the amount is not a valid input, it will show a red text beneath the text input
- If the amount is a valid input, the result section will be shown into the screen

----------
Repository
----------

The codes would also be alive in https://bitbucket.org/luqmanm/coin-changer/



import { DoValidation, DoParse, ChangeToCoins } from './CoinChangerHelper';

describe('Input Validations', () => {

  test('"123" is valid', () => {
    expect(DoValidation('123')).toHaveProperty('isValid', true);
  });

  test('"213p" is valid', () => {
    expect(DoValidation('213p')).toHaveProperty('isValid', true);
  });

  test('"£16.23p" is valid', () => {
    expect(DoValidation('£16.23p')).toHaveProperty('isValid', true);
  });

  test('"£14" is valid', () => {
    expect(DoValidation('£14')).toHaveProperty('isValid', true);
  });

  test('"£54.04" is valid', () => {
    expect(DoValidation('£54.04')).toHaveProperty('isValid', true);
  });

  test('"£23.33333" is valid', () => {
    expect(DoValidation('£23.33333')).toHaveProperty('isValid', true);
  });

  test('"001.41p" is valid', () => {
    expect(DoValidation('001.41p')).toHaveProperty('isValid', true);
  });

  test('"13x" is NOT valid', () => {
    expect(DoValidation('13x')).toHaveProperty('isValid', false);
  });

  test('"13p.02" is NOT valid', () => {
    expect(DoValidation('13p.02')).toHaveProperty('isValid', false);
  });

  test('"£p" is NOT valid', () => {
    expect(DoValidation('£p')).toHaveProperty('isValid', false);
  });

});

describe('Convert Input Into Pennies', () => {

  test('"432" is 432 Pennies', () => {
    expect( DoParse('432') ).toBe(432);
  });

  test('"213p" is 213 Pennies', () => {
    expect( DoParse('213p') ).toBe(213);
  });

  test('"£16.23p" is 1623 Pennies', () => {
    expect( DoParse('£16.23p') ).toBe(1623);
  });

  test('"£14" is 1400 Pennies', () => {
    expect( DoParse('£14') ).toBe(1400);
  });

  test('"£54.04" is 5404 Pennies', () => {
    expect( DoParse('£54.04') ).toBe(5404);
  });

  test('"£23.33333" is 2333 Pennies', () => {
    expect( DoParse('£23.33333') ).toBe(2333);
  });

  test('"001.41p" is 141 Pennies', () => {
    expect( DoParse('001.41p') ).toBe(141);
  });

  test('"£23.33333" is NOT 2333333 Pennies', () => {
    expect( DoParse('£23.33333') ).not.toBe(2333333);
  });


});

describe('Change Pennies Into Coins', () => {

	const result_123 = [
		{ 'name':'£1', 'denominator':100, 'total':1 },
		{ 'name':'20p', 'denominator':20, 'total':1 },
		{ 'name':'2p', 'denominator':2, 'total':1 },
		{ 'name':'1p', 'denominator':1, 'total':1 }
	];

	const result_1234 = [
		{ 'name':'£2', 'denominator':200, 'total':6 },
		{ 'name':'20p', 'denominator':20, 'total':1 },
		{ 'name':'10p', 'denominator':10, 'total':1 },
		{ 'name':'2p', 'denominator':2, 'total':2 }
	];

  test('123 = 1x£1, 1x20p, 1x2p, 1x1p', () => {
    expect( ChangeToCoins(123) ).toMatchObject(result_123);
  });

  test('1234 = 6x£2, 1x20p, 1x10p, 2x2p', () => {
    expect( ChangeToCoins(1234) ).toMatchObject(result_1234);
  });


});
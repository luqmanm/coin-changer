import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer'

import CoinChangerApp from './CoinChangerApp';

global.requestAnimationFrame = function(callback) {
  setTimeout(callback, 0);
};

// React 16 Enzyme adapter
Enzyme.configure({ adapter: new Adapter() });

describe('Coin Changer App', () => {

  const component = mount(
    <CoinChangerApp />
  );

  it('Renders correctly', () => {
    const component = renderer.create(
      <CoinChangerApp />
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Shows result elements when input is valid and user presses "enter"', () => {
    const result = [{ 'name':'1p', 'denominator':1, 'total':1 }];
    component.find('input').simulate('change', { target: { value: '1' } });
    component.find('input').simulate('keypress', {key: 'Enter'});
    component.update();
    expect( component.find('.result-wrapper').exists() ).toBeTruthy();
  });

  it('Shows error elements when input is NOT valid and user presses "enter"', () => {
    const result = [{ 'name':'1p', 'denominator':1, 'total':1 }];
    component.find('input').simulate('change', { target: { value: 'x' } });
    component.find('input').simulate('keypress', {key: 'Enter'});
    component.update();
    expect( component.find('div.error').exists() ).toBeTruthy();
  });

});







/* ========================================
* Validates the user input for amount value and change it to pennies.
* Returns an object containing status flag, error message, and the amount in pennies.
* @param {String} amountInput
* @return {Object} data
=========================================== */

export function DoValidation(amountInput){

	// Return value format
	let data = {
		'isValid': false,
		'msg': '',
		'totalPennies': 0			
	};

	// Trim input
	// amountInput = amountInput.trim();
	// On a second thought, do accept people putting spaces for the £ and p symbols
	amountInput = amountInput.replace(/\s+/g, '');

	// Regex rules: £ in front (0/1), 345 or 23.90, p on the back (0/1)
	const regex = /^£?\d+(?:\.\d+)?p?$/; 
	// Test the input
	const isValid = regex.test(amountInput);

	// If input format is valid
	if( isValid !== false ){
		// Try convert to pennies
		const totalPennies = DoParse(amountInput);
		// Last validation: heck if it isn't zero amount
		if( totalPennies > 0 ){
			data.isValid = true;
			data.totalPennies = totalPennies;
		}
		else{
			data.msg = 'The amount cannot be zero';
		}
	}
	else{
		data.msg = 'Invalid Format';
	}

	return data;
}


/* ========================================
* Parse the already validated user input and converts it to pennies.
* @param {String} validatedAmountInput
* @return {Integer} pennies
=========================================== */

export function DoParse(validatedAmountInput){
	// Same regex rule as Validator, but with capturing groups
	const regex = /^£?((\d+)(\.(\d+))?)p?$/;
	const regex_matches = regex.exec(validatedAmountInput);
	// Return data format
	let pennies = 0;

	// If '.' exists
	if( regex_matches[1].indexOf('.') !== -1 ){ 
		// Split the string into fractions of pound and pennies
		let fractionPound = parseInt(regex_matches[2], 10) * 100;
		let fractionPenny = parseInt(regex_matches[4], 10);

		// Just take the first two decimal
		if( regex_matches[4].length > 2 ){
			fractionPenny = parseInt( regex_matches[4].slice(0,2), 10 );
		}
		else if( regex_matches[4].length === 1 ){
			fractionPenny = fractionPenny * 10;
		}
		// Add them together
		pennies = fractionPound + fractionPenny;
	}
	else{
		// If '£' exist
		if( regex_matches[0].indexOf('£') === 0 ){
			pennies = parseInt(regex_matches[1], 10) * 100;
		}
		else{
			pennies = parseInt(regex_matches[1], 10);
		}
	}

	return pennies;
}


/* ========================================
* Calculate the coins needed to make the total amount of pennies.
* Returns an array of coin objects that contains the coin name, coin value, and total coins.
* @param {Integer} totalPennies
* @return {Array} resultCoins
=========================================== */

export function ChangeToCoins(totalPennies){
	// The coins repository, best to have descending value order
	const coins = [
		{ 'name': '£2', 'value': 200 },
		{ 'name': '£1', 'value': 100 },
		{ 'name': '50p', 'value': 50 },
		{ 'name': '20p', 'value': 20 },
		{ 'name': '10p', 'value': 10 },
		{ 'name': '5p', 'value': 5 },
		{ 'name': '2p', 'value': 2 },
		{ 'name': '1p', 'value': 1 },
	];
	let resultCoins = [];
	let currentResult = {};
	let currentCoin = 0;
	let totalCoins = 0;
	// assign the full total pennies to subtract later
	let remainingPennies = totalPennies;
	let i = 0;

	// Loop total pennies until zero
	while( remainingPennies > 0 ){
		// Set currentCoin from the biggest denominator
		currentCoin = coins[i].value;
		// If the remaining sum exceeds current denominator value
		if (remainingPennies >= currentCoin) {
			// Find how many coins needed 
			totalCoins = Math.floor(remainingPennies / currentCoin);
			// Calculate the remaining pennies 
			remainingPennies = remainingPennies - (currentCoin * totalCoins);
			// Assign result 
			currentResult = {
				'name': coins[i].name,
				'denominator': currentCoin,
				'total': totalCoins
			};
			resultCoins.push(currentResult);
		}
		// Update loop index
		i++;
	}

	return resultCoins;
}

import React from 'react';
import ReactDOM from 'react-dom';
import './kube.min.css';
import './style.css';
import CoinChangerApp from './CoinChangerApp';

ReactDOM.render(<CoinChangerApp />, document.getElementById('coin-changer-app'));

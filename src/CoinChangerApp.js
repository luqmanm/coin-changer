import React from 'react';
import {DoValidation, ChangeToCoins} from './CoinChangerHelper';


class ResultSection extends React.Component{

  render(){

  	// Don't return DOM if no resultCoins state is empty
  	if( this.props.resultCoins.length < 1 ) return null;

    // Show the result coin images
  	const show_img = this.props.resultCoins.map((coin, i) => {
  		let the_spans=[];
  		for(let x=0; x<coin.total; x++){
  			the_spans.push(<span key={"coin-img-"+x} className={"coinicon coin-"+coin.denominator}></span>);
  		}
  		return(the_spans);
  	});

    // Show result descriptions
  	const show_desc = this.props.resultCoins.map((coin, i) => {
  		return(
        <tr key={"coin-desc-"+i}>
          <td>{coin.name}</td>
          <td>{coin.total}</td>
        </tr>
  		);
  	});

    return(
      <div className="result-wrapper">

        <div className="row align-center">
          <div className="col col-12">
            <h2 className="text-center italic">results</h2>
            <div className="coins text-center">{show_img}</div>
          </div>
        </div>

        <div className="row align-center">
          <div className="coins-desc col col-2">
            <table className="striped text-center">
              <thead>
                <tr>
                  <th>Coin</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {show_desc}
              </tbody>
            </table>
          </div>
        </div>

      </div>
    );
  }

}

/* ======================================== */


class AmountInputSection extends React.Component{
  
  constructor(props){
    super(props);
    // onClicks & onChange main return function
    this.onChangeInputAmount = this.onChangeInputAmount.bind(this);
    this.onClickSubmitAmount = this.onClickSubmitAmount.bind(this);   
    this.onKeyPressSubmitAmount = this.onKeyPressSubmitAmount.bind(this);
  }

  // Track changes for value on Amount Input
  onChangeInputAmount(e){
    this.props.onChangeInputAmount(e.target.value);
  }

  // User clicks submit button
  onClickSubmitAmount(e){
    e.preventDefault();
    this.props.onClickSubmitAmount();
  }

  // User press enter on amount input
  onKeyPressSubmitAmount(e){
    if (e.key === 'Enter') this.props.onClickSubmitAmount();
  }

  // Updates the view for error message
  showErrorMessage(){
    if(this.props.formErrMsg !== ''){
      return(
        <div className="desc label tag error">Input error: { this.props.formErrMsg }</div>
      );
    }
  }

  render(){
    return(
      <div className="row align-center">
        <div className="col col-7">
          <div className="form">
            <div className="form-item">
              <label>Examples of valid inputs: 432, 213p, £16.23p, £14, £54.04, £23.33333, 001.41p</label>
              <div className="append">
                <input
                  type="text"
                  className="search big"
                  placeholder="Amount"
                  value={this.props.formInputAmount}
                  onChange={this.onChangeInputAmount}
                  onKeyPress={this.onKeyPressSubmitAmount}
                  autoFocus
                  required
                  />
                <button className="button" onClick={this.onClickSubmitAmount}>Find</button>
              </div>
              { this.showErrorMessage() }
            </div>
          </div>
        </div>
      </div>
    );
  }

}


/* ======================================== */


export default class CoinChangerApp extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      formInputAmount: '',
      formErrMsg: '',
      resultCoins: []
    };
    // Bind events from AmountInputSection Class
    this.onChangeInputAmount = this.onChangeInputAmount.bind(this);
    this.onClickSubmitAmount = this.onClickSubmitAmount.bind(this);
  }

  // Track the Amount input field value
  onChangeInputAmount(amount){
    this.setState({
      formInputAmount: amount
    });
  }

  // On Form submit
  onClickSubmitAmount(){
    // Validate input
    const validated = DoValidation(this.state.formInputAmount);
    // If input not valid
    if( validated.isValid === false ){
	  this.setState({
        // Show error messages and reset coin result
        formErrMsg: validated.msg,
        resultCoins: []
      });
      // Break the function
      return;
    }
    else{
      this.setState({
        // Reset error message
        formErrMsg: ''
      });
    }
    // The amount converted into pennies
    const totalPennies = validated.totalPennies;
    // Calculate the coins & change result state
    this.setState({
      resultCoins: ChangeToCoins(totalPennies)
    });
  }

  render(){
    return(
      <div>
        <AmountInputSection
          formInputAmount = {this.state.formInputAmount}
          formErrMsg = {this.state.formErrMsg}
          onChangeInputAmount = {this.onChangeInputAmount}
          onClickSubmitAmount = {this.onClickSubmitAmount}
        />
        <ResultSection resultCoins={this.state.resultCoins} />
      </div>
    );
  }

}


